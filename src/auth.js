import axios from 'axios'
import * as env from './env'
import * as mutationTypes from './store/mutation-types'
import _ from 'lodash'

export const getOAuth2Token = (username, password, grantType = 'password', scope = '*') => {
  return axios.post(env.OAUTH_TOKEN_URL, {
    'grant_type': grantType,
    'client_id': env.CLIENT_ID,
    'client_secret': env.CLIENT_SECRET,
    'username': username,
    'password': password,
    'scope': scope
  })
}

export const getHeader = () => {
  const authUser = JSON.parse(window.localStorage.getItem('authUser'))
  const headers = {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + authUser.access_token
  }
  return headers
}

export const initAuthUserState = (store) => {
  let authUser = JSON.parse(window.localStorage.getItem('authUser'))
  if (authUser) {
    store.commit(mutationTypes.SET_AUTH_USER, authUser)
  }
  else {
    store.commit(mutationTypes.SET_AUTH_USER, {})
  }
}

export const logoutAuthUser = (store) => {
  localStorage.removeItem('authUser')
  store.commit(mutationTypes.UNSET_AUTH_USER)
}

export const isAuthenticated = (store) => {
  return !_.isEmpty(store.state.auth.authUser)
}
