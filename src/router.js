import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store'
import * as types from './store/mutation-types'

import {isAuthenticated, initAuthUserState} from './auth'

Vue.use(VueRouter)

function load (component) {
  // '@' is aliased to src/components
  return () => System.import(`@/${component}.vue`)
}

const router = new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */
  mode: 'history',
  routes: [
    {path: '/', component: load('Home')},
    {path: '/login', name: 'login', component: load('pages/Login')},
    {
      path: '/admin',
      name: 'admin',
      component: load('layouts/Admin'),
      beforeEnter: (to, from, next) => {
        store.commit(types.UNSET_FLASH_MESSAGE_IF_DISPLAYED)
        if (!isAuthenticated(store)) {
          store.commit(types.SET_FLASH_MESSAGE, {
            displayed: false,
            message: 'Please login',
            color: 'warning'
          })
          next({name: 'login'})
        }
        else {
          next()
        }
      },
      children: [
        {
          path: 'manage-roles-permissions',
          name: 'admin.manage-roles-permissions',
          component: load('pages/AdminManageRolesPermissions')
        }
      ]
    },
    // Always leave this last one
    {path: '*', component: load('Error404')} // Not found
  ]
})

router.beforeEach((to, from, next) => {
  initAuthUserState(store)
  store.commit(types.UNSET_FLASH_MESSAGE_IF_DISPLAYED)
  next()
})

export default router
