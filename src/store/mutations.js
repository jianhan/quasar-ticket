import * as types from './mutation-types'
import _ from 'lodash'

let mutations = {
  [types.SET_FLASH_MESSAGE] (state, flashMessage) {
    state.flashMessage = flashMessage
  },
  [types.UNSET_FLASH_MESSAGE] (state) {
    state.flashMessage = {}
  },
  [types.SET_FLASH_MESSAGE_DISPLAYED] (state, displayed) {
    state.flashMessage['displayed'] = displayed
  },
  [types.UNSET_FLASH_MESSAGE_IF_DISPLAYED] (state) {
    if (_.has(state, 'flashMessage.displayed') && state.flashMessage.displayed === true) {
      state.flashMessage = {}
    }
  }
}

export default mutations
