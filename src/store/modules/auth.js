import * as types from '../mutation-types'

// initial state
const state = {
  authUser: {}
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
  [types.SET_AUTH_USER] (state, authUser) {
    state.authUser = authUser
  },
  [types.UNSET_AUTH_USER] (state) {
    state.authUser = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
