import _ from 'lodash'

export const isFlashMessageSet = state => {
  return !_.isEmpty(state.flashMessage)
}
